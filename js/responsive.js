var defaultURL = 'google.cl'; //<---- CHANGE TO YOUR WEBSITE URL

//show loading graphic
function showLoader(id) {
  $('#' + id + ' img').fadeIn('slow');
}

//hdie loading graphic
function hideLoader(id) {
  $('#' + id + ' img').fadeOut('slow');
}

//function to check load state of each frame
function allLoaded(){
  var results = [];
  $('iframe').each(function(){
    if(!$(this).data('loaded')){results.push(false)}
  });
  var result = (results.length > 0) ? false : true;
  return result;
};

function loadPage($frame, url) {
  if ( url.substr(0,7) !== 'http://' && url.substr(0,8) !== 'https://' && url.substr(0, 7) !== 'file://' ) {
    url = 'https://'+url;
  }
  $('iframe').not($frame).each(function(){showLoader($(this).parent().attr('id'));})
  $('iframe').not($frame).data('loaded', false);
  $('iframe').not($frame).attr('src', url);
}

$('.frame').each(function(){showLoader($(this).attr('id'))});


//when document loads
$(document).ready(function(){

  loadPage('', defaultURL);

  //query string
  var qsArray = window.location.href.split('?');
  var qs = qsArray[qsArray.length-1];

  if(qs != '' && qsArray.length > 1){
    $('#url input[type=text]').val(qs);
    loadPage('', qs);
  }

  //set slidable div width
  $('#frames #inner').css('width', function(){
    var width = 0;
    $('.frame').each(function(){width += $(this).outerWidth() + 20});
    return width;
  });

  $('#equalHeight').change(function(){
    if($(this).is(":checked")) {
        $('#frames').addClass("equalHeight");
        $('#frames').addClass("widthOnly");
    } else {
        $('#frames').removeClass("equalHeight");
        $('#frames').removeClass("widthOnly");

    }
  });

  //when the url textbox is used
  $('form').submit(function(){
    loadPage('' , $('#url input[type=text]').val());
    return false;
  });

  //when frame loads
  $('iframe').load(function(){

    var $this = $(this);
    var url = '';
    var error = false;

    try{
      url = $this.contents().get(0).location.href;
    } catch(e) {
      error = true;
      if($('#url input[type=text]').val() != ''){
        url = $('#url input[type=text]').val();
      } else {
        url = defaultURL;
      }
    } 

    //load other pages with the same URL
    if(allLoaded()){
      if(error){
        alert('Browsers prevent navigation from inside iframes across domains.\nPlease use the textbox at the top for external sites.');
        loadPage('', defaultURL);
      }else{
        loadPage($this, url);
      }
    }

    //when frame loads, hide loader graphic
    else{
      error = false;
      hideLoader($(this).parent().attr('id'));
      $(this).data('loaded',true);
    }
  });

});
$( ".frame__button--url-submit" ).click(function(){
  lastUrl = $( "#url input" ).val();
  setLastUrl();
});

function setLastUrl(){
  Cookies.set('RWD_lastUrl', {
    lastUrl: lastUrl,
  });
}

function getLastUrl(){
  var RWD_lastUrl = JSON.parse(Cookies.get('RWD_lastUrl'));
  cookieLastUrl = RWD_lastUrl.lastUrl;

  $( "#url input" ).val(cookieLastUrl);
}

$( "#resetSizes" ).click(function(){
    Cookies.remove('RWD_sizeConfig');
    location.reload();
});

$( "#applyChangeSize" ).click(function() {
    valWidthFrame__one = $( ".frame__width--one-value" ).val();
    valWidthFrame__two = $( ".frame__width--two-value" ).val();
    valWidthFrame__three = $( ".frame__width--three-value" ).val();
    valWidthFrame__four = $( ".frame__width--four-value" ).val();
    valWidthFrame__five = $( ".frame__width--five-value" ).val();
    valWidthFrame__six = $( ".frame__width--six-value" ).val();
    valWidthFrame__seven = $( ".frame__width--seven-value" ).val();

    $("iframe.iframe__item--one").width(valWidthFrame__one);
    $("iframe.iframe__item--two").width(valWidthFrame__two);
    $("iframe.iframe__item--three").width(valWidthFrame__three);
    $("iframe.iframe__item--four").width(valWidthFrame__four);
    $("iframe.iframe__item--five").width(valWidthFrame__five);
    $("iframe.iframe__item--six").width(valWidthFrame__six);
    $("iframe.iframe__item--seven").width(valWidthFrame__seven);

    $(".frame__width--one").text(valWidthFrame__one);
    $(".frame__width--two").text(valWidthFrame__two);
    $(".frame__width--three").text(valWidthFrame__three);
    $(".frame__width--four").text(valWidthFrame__four);
    $(".frame__width--five").text(valWidthFrame__five);
    $(".frame__width--six").text(valWidthFrame__six);
    $(".frame__width--seven").text(valWidthFrame__seven);

    valHeightFrame__one = $( ".frame__height--one-value" ).val();
    valHeightFrame__two = $( ".frame__height--two-value" ).val();
    valHeightFrame__three = $( ".frame__height--three-value" ).val();
    valHeightFrame__four = $( ".frame__height--four-value" ).val();
    valHeightFrame__five = $( ".frame__height--five-value" ).val();
    valHeightFrame__six = $( ".frame__height--six-value" ).val();
    valHeightFrame__seven = $( ".frame__height--seven-value" ).val();

    $("iframe.iframe__item--one").height(valHeightFrame__one);
    $("iframe.iframe__item--two").height(valHeightFrame__two);
    $("iframe.iframe__item--three").height(valHeightFrame__three);
    $("iframe.iframe__item--four").height(valHeightFrame__four);
    $("iframe.iframe__item--five").height(valHeightFrame__five);
    $("iframe.iframe__item--six").height(valHeightFrame__six);
    $("iframe.iframe__item--seven").height(valHeightFrame__seven);

    $(".frame__height--one").text(valHeightFrame__one);
    $(".frame__height--two").text(valHeightFrame__two);
    $(".frame__height--three").text(valHeightFrame__three);
    $(".frame__height--four").text(valHeightFrame__four);
    $(".frame__height--five").text(valHeightFrame__five);
    $(".frame__height--six").text(valHeightFrame__six);
    $(".frame__height--seven").text(valHeightFrame__seven);

    labelFrame__one = $( ".frame__label-text--one" ).val();
    labelFrame__two = $( ".frame__label-text--two" ).val();
    labelFrame__three = $( ".frame__label-text--three" ).val();
    labelFrame__four = $( ".frame__label-text--four" ).val();
    labelFrame__five = $( ".frame__label-text--five" ).val();
    labelFrame__six = $( ".frame__label-text--six" ).val();
    labelFrame__seven = $( ".frame__label-text--seven" ).val();
    
    $(".frame__label--one").text(labelFrame__one);
    $(".frame__label--two").text(labelFrame__two);
    $(".frame__label--three").text(labelFrame__three);
    $(".frame__label--four").text(labelFrame__four);
    $(".frame__label--five").text(labelFrame__five);
    $(".frame__label--six").text(labelFrame__six);
    $(".frame__label--seven").text(labelFrame__seven);

    storeData();
    
});

function storeData(){
  Cookies.set('RWD_sizeConfig', {
      valWidthFrame__one: valWidthFrame__one,
      valWidthFrame__two: valWidthFrame__two,
      valWidthFrame__three: valWidthFrame__three,
      valWidthFrame__four: valWidthFrame__four,
      valWidthFrame__five: valWidthFrame__five,
      valWidthFrame__six: valWidthFrame__six,
      valWidthFrame__seven: valWidthFrame__seven,

      valHeightFrame__one: valHeightFrame__one,
      valHeightFrame__two: valHeightFrame__two,
      valHeightFrame__three: valHeightFrame__three,
      valHeightFrame__four: valHeightFrame__four,
      valHeightFrame__five: valHeightFrame__five,
      valHeightFrame__six: valHeightFrame__six,
      valHeightFrame__seven: valHeightFrame__seven,

      labelFrame__one : labelFrame__one,
      labelFrame__two : labelFrame__two,
      labelFrame__thre : labelFrame__three,
      labelFrame__four : labelFrame__four,
      labelFrame__five : labelFrame__five,
      labelFrame__six : labelFrame__six,
      labelFrame__seve : labelFrame__seven,
  });
}

function getData(){
  var RWD_sizeConfig = JSON.parse(Cookies.get('RWD_sizeConfig'));
  
  cookieWidthFrame__one = RWD_sizeConfig.valWidthFrame__one;
  cookieWidthFrame__two = RWD_sizeConfig.valWidthFrame__two;
  cookieWidthFrame__three = RWD_sizeConfig.valWidthFrame__three;
  cookieWidthFrame__four = RWD_sizeConfig.valWidthFrame__four;
  cookieWidthFrame__five = RWD_sizeConfig.valWidthFrame__five;
  cookieWidthFrame__six = RWD_sizeConfig.valWidthFrame__six;
  cookieWidthFrame__seven = RWD_sizeConfig.valWidthFrame__seven;

  cookieHeightFrame__one = RWD_sizeConfig.valHeightFrame__one;
  cookieHeightFrame__two = RWD_sizeConfig.valHeightFrame__two;
  cookieHeightFrame__three = RWD_sizeConfig.valHeightFrame__three;
  cookieHeightFrame__four = RWD_sizeConfig.valHeightFrame__four;
  cookieHeightFrame__five = RWD_sizeConfig.valHeightFrame__five;
  cookieHeightFrame__six = RWD_sizeConfig.valHeightFrame__six;
  cookieHeightFrame__seven = RWD_sizeConfig.valHeightFrame__seven;

  cookieLabelFrame__one = RWD_sizeConfig.labelFrame__one;
  cookieLabelFrame__two = RWD_sizeConfig.labelFrame__two;
  cookieLabelFrame__three = RWD_sizeConfig.labelFrame__three;
  cookieLabelFrame__four = RWD_sizeConfig.labelFrame__four;
  cookieLabelFrame__five = RWD_sizeConfig.labelFrame__five;
  cookieLabelFrame__six = RWD_sizeConfig.labelFrame__six;
  cookieLabelFrame__seven = RWD_sizeConfig.labelFrame__seven;


  $("iframe.iframe__item--one").width(cookieWidthFrame__one);
  $("iframe.iframe__item--two").width(cookieWidthFrame__two);
  $("iframe.iframe__item--three").width(cookieWidthFrame__three);
  $("iframe.iframe__item--four").width(cookieWidthFrame__four);
  $("iframe.iframe__item--five").width(cookieWidthFrame__five);
  $("iframe.iframe__item--six").width(cookieWidthFrame__six);
  $("iframe.iframe__item--seven").width(cookieWidthFrame__seven);

  $(".frame__width--one").text(cookieWidthFrame__one);
  $(".frame__width--two").text(cookieWidthFrame__two);
  $(".frame__width--three").text(cookieWidthFrame__three);
  $(".frame__width--four").text(cookieWidthFrame__four);
  $(".frame__width--five").text(cookieWidthFrame__five);
  $(".frame__width--six").text(cookieWidthFrame__six);
  $(".frame__width--seven").text(cookieWidthFrame__seven);

  $("iframe.iframe__item--one").height(cookieHeightFrame__one);
  $("iframe.iframe__item--two").height(cookieHeightFrame__two);
  $("iframe.iframe__item--three").height(cookieHeightFrame__three);
  $("iframe.iframe__item--four").height(cookieHeightFrame__four);
  $("iframe.iframe__item--five").height(cookieHeightFrame__five);
  $("iframe.iframe__item--six").height(cookieHeightFrame__six);
  $("iframe.iframe__item--seven").height(cookieHeightFrame__seven);

  $(".frame__height--one").text(cookieHeightFrame__one);
  $(".frame__height--two").text(cookieHeightFrame__two);
  $(".frame__height--three").text(cookieHeightFrame__three);
  $(".frame__height--four").text(cookieHeightFrame__four);
  $(".frame__height--five").text(cookieHeightFrame__five);
  $(".frame__height--six").text(cookieHeightFrame__six);
  $(".frame__height--seven").text(cookieHeightFrame__seven);
  
  $(".frame__label--one").text(cookieLabelFrame__one);
  $(".frame__label--two").text(cookieLabelFrame__two);
  $(".frame__label--three").text(cookieLabelFrame__three);
  $(".frame__label--four").text(cookieLabelFrame__four);
  $(".frame__label--five").text(cookieLabelFrame__five);
  $(".frame__label--six").text(cookieLabelFrame__six);
  $(".frame__label--seven").text(cookieLabelFrame__seven);
}

$(document).ready(function () {
  if ( Cookies.get('RWD_lastUrl') !== undefined ) {
      getLastUrl();
  }
  if ( Cookies.get('RWD_sizeConfig') !== undefined ) {
    getData();
}
});